let snowflakes = []; // array to hold snowflake objects

function setup() {
  // put setup code here
  createCanvas(640,480);

}
let value = 51;
function draw() {
  // put drawing code here

// The background - the black sky
  background(51);
  let t = frameCount / 60; // update time

// create a random number of snowflakes each frame
  for (let i = 0; i < random(5); i++) {
    snowflakes.push(new snowflake()); // append snowflake object
  }

  // loop through snowflakes with a for..of loop
  for (let flake of snowflakes) {
    flake.update(t); // update snowflake position
    flake.display(); // draw snowflake
  }


// the color of the foundation of the house
    fill('red');
//the foundation of the house
    square(120,150,250);

// the color of the roof
    fill(150);
//the roof of the house
    triangle(390,150,250,100,100,150);

//the color of the outline of the house
  stroke('blue');

//the color of the three windows - yellow
    fill(value);
//the 3 windows
    square(145,180,90);
    square(255,180,90);
    square(255,300,90);

//the color of the door
  fill('blue');
 // A rectangle - the door
  rect(160, 300, 60, 100);

//the color of the doorknob
fill(255);
// An ellipse - the doorknob
ellipse(170, 350, 7, 7);

//the color of the ground - snow
    fill(255);
 // A rectangle - the ground
  rect(0, 400, 640, 80);
}
function mousePressed() {
  if (value === 51) {
    value = color(255, 204, 0);
  } else {
    value = 51;
  }
}

// snowflake class
function snowflake() {
  // initialize coordinates
  this.posX = 0;
  this.posY = random(-50, 0);
  this.initialangle = random(0, 2 * PI);
  this.size = random(2, 5);

  // radius of snowflake spiral
  // chosen so the snowflakes are uniformly spread out in area
  this.radius = sqrt(random(pow(width / 2, 2)));

  this.update = function(time) {
    // x position follows a circle
    let w = 0.6; // angular speed
    let angle = w * time + this.initialangle;
    this.posX = width / 2 + this.radius * sin(angle);

    // different size snowflakes fall at slightly different y speeds
    this.posY += pow(this.size, 0.5);

    // delete snowflake if past end of screen
    if (this.posY > height) {
      let index = snowflakes.indexOf(this);
      snowflakes.splice(index, 1);
    }
  };

  this.display = function() {
//// to make the outline of the snowflakes white (instead of blue like the rest)
  stroke(255);
  ellipse(this.posX, this.posY, this.size);
  };
}
