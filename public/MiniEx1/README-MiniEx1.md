![ScreenShot](Screenshot-MiniEx1.png)
Link to RUNME: https://AnneNielsen.gitlab.io/ap-2020-projects/MiniEx1/ 

**README – MiniEx1**

For this MiniEx the objective was:

*  To learn the basic setup, including writing code with a code editor, running code with a web browser, independent study of code syntax, creating a readme file, etc.
*  To reflect upon the process of coding.


**Idea**

My first idea was to create a house in the dark with blinking lights in the windows – to represent the winter season, my favorite.
In my process, I decided to involve the user more in the interface and added the function “mousePressed()” – then the user actively can turn the lights on and off in the windows, instead of the constantly blinking lights.

The final product is a programmed red house with a grey roof, a blue door, yellow lights in three windows (you can turn off and on by clicking), a blue outline, snow on the ground, a black background, and snow in the sky (on the background).  


**My experience**

My first independent coding experience was in general fun! I found it interesting to try out different codes, getting into that way of thinking, and combining codes to make a visualization, picture or artwork. Though I was it very frustrating, when the codes didn’t “match” one another (e.g. wrong place to clip codes in) and couldn’t work together – but after looking at it for a while, I found the “mistake” and made it work. When you make it work, it is so incredibly satisfying – the reason why I now like coding. I also find it very cool, that you can visualize an idea by typing in different numbers.

There is a lot of similarities between natural language and programming language (when coding). You can read and write both languages. Some words in programming language is the same as in the natural language,- programming language just contains a lot of numbers, “signs”, functions, etc. as well.
At first I found it very difficult to understand programming language, and combine the codes to a language, the computer could read and understand. Once you dig deeper into it, it starts to make sense and gets really fun.

References and examples on p5.js is an amazing help as well, they make the bridge from the one language to the other easier,- it is basically a library to translate between the two (https://p5js.org/reference/ & https://p5js.org/examples/) .

Like the overall message in the assigned readings says (Nick Montfort, 2016; Annette Vee, 2017; Lauren McCarthy, 2015; Daniel Shiffman, 2018): Don’t be scared, just start and try things out. Coding is for everyone, and it is the future.




**Programming diary – my process in programming for miniEX1**

*  I started off by entering the basic programming sketch, we got in the first class in AP - “sketch01.js” (https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/public/sketch01/sketch01.js) .
*  Then I added the the color black as background – symbolizing the dark night (https://p5js.org/reference/#/p5/background)
*  Then I added a box, that should create the foundation of my house (https://p5js.org/reference/#/p5/square).
    *  Then I colored my house red – symbolizing the many red brick houses in Denmark (https://p5js.org/reference/#/p5/fill).
*  Then I found a triangle to make the roof (https://p5js.org/reference/#/p5/triangle)
    *  Made it the color gray with the code: “fill(150);”
*  Then I added 3 more boxes, and placed them on the house as windows
(https://p5js.org/reference/#/p5/square).
    *  Afterwards I made them yellow with the code “(fill(225,204,0);” - (https://p5js.org/reference/#/p5/fill).
        *  But I wanted the windows “twinkling” with yellow light, and started experimenting with “random” where it looked a bit like twinkling.
*  I made the windows “blink” the color yellow really fast by entering the code “fill(255,random(275),0);”
    *  I was not satisfied with this and decided, that you actively should click on the windows to make them turn on the yellow lights.
*  To make the lights in the house turn on an off, I found the function “mousePressed()” and added the function to the three windows (https://p5js.org/reference/#/p5/mousePressed).
    *  Now when you press on one of the three windows, “the lights turn on”.
*  I got inspired by my fellow students to outline the house with blue, to make it more like a modern artwork and typed in the code: “stroke(‘blue’);”
*  Then I added the door to the house, a rectangle (https://p5js.org/examples/hello-p5-simple-shapes.html)
    *  Made it the color blue, to make it stand out from the other parts of the house – code: “fill(‘blue’);”.
    *  Then I added a doorknob (an ellipse - https://p5js.org/examples/hello-p5-simple-shapes.html) and made it white (code: “fill(255);”).
*  One of my fellow students found a code to make snowflakes, and I added that since my theme is winter: https://p5js.org/examples/simulate-snowflakes.html.
    *  When I added the snowflakes, my outline with blue, was around the snowflakes as well – I fixed that by adding the code “stroke(255);” just before the creation of the ellipse (under “snowflake class”).
*  With the snow in the background, I needed a snowy ground as well, so I made a big rectangle (https://p5js.org/examples/hello-p5-simple-shapes.html)
    *  I made the ground white – color code: “fill(225);”


