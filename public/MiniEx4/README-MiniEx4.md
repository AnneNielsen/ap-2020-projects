**README – MiniEx4**


For this MiniEx the objective was:

*  To experiment with various data capturing inputs, including audio, mouse, keyboard, web camera, and more.
*  To critically reflect upon the activity of data capture and datafication.


URL link to my repository: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public%2FMiniEx4


**This week’s MiniEx is a submission for the Transmediale Festival with the theme “Capture All”.**


The title for my work is “Click Capture-Sensation”. Click Capture-Sensation represents the tracking culture we live in, that just one click or mouse move contributes to the process of capturing life in data - every move and click we make.
When you take a look at my work, you first see a button saying “click me” – it looks plain, simple and you wouldn’t think there is any danger behind it. When you click the button, the picture changes, and you are being tracked and traced through clicking, video tracing your facial points, audio and every mouse move (even when you hold still) – you are processing data to capture.
My works purpose is to make people aware of how much we are being tracked, traced and captured in our everyday lives. That a click on a button can involve being captured and saying yes to your data being used. Do you like being captured?


When taking a close look at my code, you will notice the press “C” to refresh the page. It represents that there might be a way out of the capturing, but you might only be able to figure it out as a programmer – can you make it back to the page where it all started, the button?


My work is build up on several layers of interacting , capturing data from the user.
I got a lot of inspiration to my work from Winnie Soons code (https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/public/sketch05/sketch05.js) and the text "Button" by Søren Pold, talking about the buttons role in society and the potential software behind it.
In my work I use several user interacting elements: audio, video, tracking of your facial points (through video), key pressed, mouse pressed, mouse movements and a button.
Through Winnie Soons code, I have learned how to incorporate video, facial tracking, key pressed, audio and buttons(and styling of them) into my coded work. I have played a bit around with her code, removed and added different elements.
I got the idea to create a startup page with just a button, that should “activate it all” through Magic Monk on YouTube (https://www.youtube.com/watch?v=TgHhEzKlLb4) – so I added the “mode function” – almost works like an on and off button. This element represent the innocent look of a button, but it can really be powerful and contain a lot of things, we often aren’t aware of.
In playing around with Winnie Soons code, I had a lot of trouble with layering elements on the video, especially doing facial-recognition. The facial tracking points wouldn’t draw on top of the video, since the video capture work on a different layer than the canvas (different layering settings). Therefore I layered the canvas on top of the video.

This work addresses the theme “Capture all” by trying to create an awareness around all the data being captured, often without people knowing. This work is made as a response to all the “easy click” buttons e.g. the button you have to click when accepting terms and conditions – one simple click and you are saying yes to a whole lot of stuff,- you’re then in the loop of being captured and tracked.
A button can contain a lot of power – do you know what you accept and what you are getting into by clicking it? Data capture is a difficult topic to grasp. All interactions with a computer is being captured into data. The big question is, what your data is being used for, and if you are aware of how much data you really produce to be used? By clicking a button, what do you say yes to? By entering a website, what do you say yes to? Just by moving your mouse in a webpage, you are being captured, producing data.


Is it possible for us users to be aware of the data we are producing and what it is being used for – can we get that control over our own actions?


![ScreenShot](Screenshot-MiniEx4-captureall.png)

Link to RUNME: https://AnneNielsen.gitlab.io/ap-2020-projects/MiniEx4/ 


**Sources**


*  Søren Pold, "Button", in Matthew Fuller, ed. Software Studies (Cambridge, Mass.: MIT Press 2008): 31 – 36 (6 pages in pdf p. 46 – 51).
*  Winnie Soon – her programmed work: https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/public/sketch05/sketch05.js

In programming:

*  To create the key pressed to make it  clear the screen: https://p5js.org/reference/#/p5/keyPressed & https://p5js.org/reference/#/p5/clear
*  To find the keycode for C: http://keycode.info/
*  To track the face I got inspiration from Winnie soon, who used the clmtracker: https://github.com/auduno/clmtrackr & her code: https://gitlab.com/siusoon/aesthetic-programming/-/blob/master/public/sketch05/sketch05.js
*  I used this source to get inspiration to make the button activate a whole series of code: https://www.youtube.com/watch?v=TgHhEzKlLb4
