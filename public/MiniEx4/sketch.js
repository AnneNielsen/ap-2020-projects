/*
//Interacting with captured data: Mouse, Keyboard, Audio, Web Camera
check:
1. sound input via microphone: https://p5js.org/examples/sound-mic-input.html
2. dom objects like button
3. p5.sound library: https://github.com/processing/p5.js-sound/blob/master/lib/p5.sound.js
4. Face tracking library: https://github.com/auduno/clmtrackr
5. p5js + clmtracker.js: https://gist.github.com/lmccart/2273a047874939ad8ad1
*/


//defining functions
let button;
let mic; //microphone
let ctracker; //tracking the face
let capture; //to create a new HTML5 <video> element that contains the audio/video feed from a webcam, but runs seperate from the canvas: https://p5js.org/reference/#/p5/createCapture
var mode; //determine whether the program has started
let c; // canvas
var cleared = false; //we only want the canvas to clear once, so we have to make a varriable for it not to clear every frame/time it runs.

function setup() {
  mode = 0;// initialy the program has not started

  background(100); //canvas color - not really necessary, since we cant see the canvas for the video on top.

  // Audio capture
  mic = new p5.AudioIn();
  mic.start();

  //web cam capture
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.position(0,0);

  c = createCanvas(640, 480);
  c.position(0,0);

  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //creation of the button and the function of it, mousePressed
  button = createButton('CLICK ME');
  button.position(19, 19);
  button.mousePressed(changeMode);

  //styling of the button - the point is that the button should look innocent, not anything wild or dangerours to click
    button.style("font-size", "10px");
    button.style("font-weight", "200");
    button.style("padding", "10px","0","7.5px");
    button.style("-webkit-transition","all 0.2s","ease 0s")

}

function draw() {

  //To turn the system on through the program we need an "on and off" mode (0 and 1).
  console.log(mode);

  //this is the mode where only the button is shown (off mode - the program is off).
  if (mode==0) {
    background(255);
  }

  //this is the mode where video, audio and text is shown (on mode - the program is on).
  if (mode==1) {
    if(cleared == false) {
      clear();
      cleared = true;
    }
    //activating the tracker in the program
    tracker();

    //text following the mouse
    fill(255);
    //edit text here
    textSize(11);
    text("YOURE CAPTURED", mouseX, mouseY, 150, 150);

    //the rectangle showing when you press the mouse (captures the text - to underline my statement).
    if (mouseIsPressed) {
      noFill();
      stroke(255, 0, 0, 10);
      strokeWeight(10);
      rect(mouseX, mouseY, 110, 10);
      }
  }
}

//the button turning the system "on"
function changeMode() {
  if (mode == 0) { //click on the button
    mode = 1;
  }
}

//keyboard reload
function keyPressed() {
  if (keyCode === 67) { //click "C" - check here: http://keycode.info/
    location.reload();
  }
}

//setup face tracker - the red dots capturing your face.
function tracker() {

  //getting the audio data
  let vol = mic.getLevel(); // Get the overall volume (between 0 and 1.0)
  let positions = ctracker.getCurrentPosition();
  if (positions.length) { //check the availability of web cam tracking
    console.log("tracking!");

    for (let i=0; i<positions.length; i++) {  //loop through all major face track points (see: https://www.auduno.com/clmtrackr/docs/reference.html)
      noStroke();
      fill(map(positions[i][0], 0, width, 100, 255), 0,0,10);  //color with alpha value
      //draw ellipse at each position point
      ellipse(positions[i][0], positions[i][1], 5, 5);
    }
  }
}
