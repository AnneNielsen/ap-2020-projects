function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(240);

//Dark skinned smiley face

  //the ellipse of the face and skin color
  fill(120,70,0); // the interior of the ellipse - color
  stroke(110,50,0,240); //the color of the outline (RGBA)
  strokeWeight(10); // how thick the outline is
  ellipse(200, 200, 275, 275);

  //outline for the eyes and the mouth
  stroke(80,30,0); //the color of the outline (RGB)
  strokeWeight(1); //the thickness of the outline

  //the eyes
  fill(60,20,0); // the interior of the ellipse - color
  ellipse(160, 150, 30, 80);
  fill(60,20,0) // the interior of the ellipse - color
  ellipse(240, 150, 30, 80);


  //the mouth
  fill(255) // the interior of the ellipse - color
  arc(200, 225, 175, 150, 0, PI + TWO_PI, CHORD);
  fill(60,20,0) // the interior of the ellipse - color
  arc(200, 240, 170, 120, 0, PI + TWO_PI, CHORD);
  // CHORD = closes the ellipse with an outline
  //TWO_PI = "cuts the ellipse up", so its not a full ellipse

}
