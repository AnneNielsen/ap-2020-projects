let bubbles = [];

let fist;
let peace;
let flat;

var hands = [];

function preload() {
  //Classifier for the video
  classifier = ml5.imageClassifier(imageModelURL + 'model.json');

  //Loading pictures
  fist = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/89782582_563772680902418_1667800891266170880_n.png?_nc_cat=109&_nc_sid=b96e70&_nc_oc=AQlOTfTjmXEh3OZ223m56faATYEZAeZpSka0m2mZE0dmM1BJ9DuOJyArSk013kwC60g&_nc_ht=scontent.faar2-1.fna&oh=b9a6afe8ebe1ff10ab922b150b2b07df&oe=5E941254");
  peace = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/89819276_200350674586604_182835976106672128_n.png?_nc_cat=106&_nc_sid=b96e70&_nc_oc=AQluyN8lE8ycgYlCL-gXbVUR23coN7mHvXYWPW4GR4s0fxRmJ6DEW6nBtHm9Mor8Zac&_nc_ht=scontent.faar2-1.fna&oh=b103e2af475ae11fabf4f48aa547480f&oe=5E940295");
  flat = loadImage("https://scontent.faar2-1.fna.fbcdn.net/v/t1.15752-9/89829980_518194112444972_841348303323922432_n.png?_nc_cat=101&_nc_sid=b96e70&_nc_oc=AQmE9mjLY554_4exwAiAQ56xNUF2aMl2o8A8Kg39DYw7AFjh4daD5956GZf_T90V9js&_nc_ht=scontent.faar2-1.fna&oh=0839150ec2234c9266c9063a2e2c5968&oe=5E929221");
  hands[0] = fist;
  hands[1] = peace;
  hands[2] = flat;

}

function setup() {
  //Code for the VIDEO GAME
  createCanvas(400+400, 400); //to make the canvas the doubble width - then both my sketchs can run beside one another
  video = createCapture(VIDEO);
  video.size(400, 400);
  video.hide();

  flippedVideo = ml5.flipImage(video)
  classifyVideo();
  frameRate(2); //so you will be able to see the signs for hand signals the computer does - circle, rect and an angle bracket

//Code for the flying hand signals
  for(let i = 0; i < 200; i++) {
    let x = random (400, width);
    let y = random(height);
    let r = random(30,50);
    let hand = random(hands); //picking a random object (random picture)
    let b = new Bubble(x,y,r,hand);
    bubbles.push(b);
    }
}

function draw() {
  background(0);
  for (let i = 0; i < bubbles.length; i++) {
    bubbles[i].move();
    bubbles[i].show();
  }
//TO DRAW THE VIDEO
  image(flippedVideo, 0, 0, 400, 400);

  /************************
    Add class logic here
  ************************/

//Text that shows what class the computer think it is
  fill(255);
  textSize(20);
  text(label, 30, 30);
}

//Creathing the class to create the images
class Bubble {
  constructor(x,y,r,hand) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.hand = hand;
  }

//to make the picture more "alive", I made the bubbles move a little
  move() {
    this.x = this.x + random(-6,6);
    this.y = this.y + random(-6,6);
  }

//To make the bubbles vissible, we add show and write the code to create how they should look.
  show() {
//TO MAKE A FUNCTION FOR ALL HAND SIGNALS
    image(this.hand, this.x,this.y, this.r, this.r); //image, x, y, width, height

  }
}
