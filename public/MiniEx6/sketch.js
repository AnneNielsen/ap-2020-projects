//Defining functions

//MODEL FROM TEACHABLE MACHINES
let modelId = 'lE1HxNrl'
let classifier;
//the image classifier model I made in the program.
let imageModelURL = `https://teachablemachine.withgoogle.com/models/rTGEs-v5/`;

let video; //Allowing video
let flippedVideo;
let label = ""; //An array for the labels I have created in the online program "stone", "scissor", "paper"


//FUNCTION PRELOAD IS IN THE OTHER SKETCH

//FUNCTION SETUP IS USED IN THE OTHER SKETCH

//FUNCTION DRAW IS USED IN THE OTHER SKETCH


function classifyVideo() {
  flippedVideo = ml5.flipImage(video)
  classifier.classify(flippedVideo, gotResult);
}

function gotResult(error, results) {
  if (error) {
    console.error(error);
    return;
  }
  label = results[0].label;
  classifyVideo();

//Showing how it will "defeat" your symbol
  if (label == "scissor") {
    fill(20, 20, 100, 20);
    ellipse(200,200,100);
    }
  if (label == "stone") {
    fill(20, 20, 100,20);
    rect(150,200,100,40);
    }
  if (label == "paper") {
    textSize(200);
    text("<",200,200);
    }
}
