README – MiniEx6

**Games with objects**

URL link to my repository: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public%2FMiniEx6

**For this MiniEx the objective was:**

*  To implement a class-based object oriented sketch via abstracting and designing objects' properties and behaviors.
*  To reflect upon object abstraction under the conditions of digital culture

![ScreenShot](Screenshot-StoneScissorPaper.png)

Link to RUNME: https://AnneNielsen.gitlab.io/ap-2020-projects/MiniEx6/ 

**OBS!** The program works best, if you only show your hands to the camera (not your face).


**My idea**

My vision was to create an interaction, a game, between computer and human. The computer is very “static” and “locked” in its way of thinking, the “*…programming language is a carefully and precisely constructed set of protocols established in view of historically, technically, organizationally etc…*” ( Fuller & Goffey, 2017, p. 3). 
We often think of the computer as a perfect element, and that it is us humans that mess things up, if a program doesn’t work,- but that is not necessarily the case, computers can mess up and make mistakes too (e.g. errors, pixel mistakes, glitches etc. ). That is the message I want to give in this game called “stone, scissor, paper” (like the original old-fashioned kids game), where you play against the computer. 
This creation, can also illustrate the translation from human signals and expressions into programming language, code and the computers way of communicating – “*Relatively speaking, humans will adapt to – or at least learn not to notice – the stupidities of the computer much more quickly than the computer will to humans…*” ( Fuller & Goffey, 2017, p. 7).

I found this week’s MiniEx a big challenge, since I don’t really enjoy a lot of the “classical” digital games like “flappy bird”, “snake” etc. 
I therefore chose to go a bit more old school and created a digital version of the old-fashioned kids game with hand signals – “Stone, scissor, paper”. 
Normally you battle two people against each other by, on the count of 3, showing one of the three possible hand signals.
The first hand signal “stone” is visualized by a closed fist, the second “scissor” is shown by making some sort of sideways peace hand signal (two fingers pointing out) and the third signal “paper” is visualized by a flat hand (a sideways high-five). 


The rules for the game are the following: 

*  Paper wins over stone – wraps it in. 
*  Stone wins over scissor – breaks it. 
*  Scissor wins over paper – cuts it over.


If the computer reads your hand signals through the camera correctly, it wins - but if it doesn’t, then you win. 

The computers hand signals are symbols:

*  stone = a circle.
*  scissor = an angle bracket (less than).
*  paper = a rectangle.

I didn’t know how to but the pictures of the hand signals inside an if statement,- so computer symbols will have to do for now. 

I have drawn a lot of inspiration from the workshop program “Teachable Machine”, we learned at the PCD event the 22nd of February. I have used this program to make the camera read the hand signals you make (image recognition).


**Object-oriented programming**

This week’s MiniEx we had to incorporate a class-based object oriented approach. I struggled quite a lot with this, and had a hard time incorporating it into my game. That is why I have added it as a side element to the actual game,- to show that I know how to do the basics of a class-based object approach, but that I didn’t know how to incorporate it better in this context. 
I made an array for hand signal pictures, then I chose them to have a random size, placing, and a small movement (to make it look more alive). 

Object-oriented programming is really smart to use, if you want a lot of the same elements or should I say “class” in your program. An object is build up on several layers of abstraction, -which means there is a lot of processes behind an object  - e.g. the application of it, the algorithm behind it, the programming language you write it in (for the computer to understand), the instructions set architecture (the look of it), its behaviors, the display of it etc..
When you need the same function and syntaxes in object-oriented programming (the use of classes), there is an easier way and shorter code to write,- which is brilliant. But for my approach to this week’s MiniEx, this class-based object oriented approach didn’t help me a lot. 

**OBS!** To create a better overview, I have two sketches (.js files) – “Bubble.js” & “sketch.js”.


**Sources**


*  Matthew Fuller & Andrew Goffey, "The Obscure Objects of Object Orientation", in Matthew Fuller, ed. How to be a Geek: Essays on the Culture of Software(Cambridge: Polity, 2017).
*  The program I have used to combine movements to the showing objects (used from PCD event 22. Feb – Machine Learning with P5.js) 
    *  Tutorial, libraries ect.: https://github.com/nckn/PCDAarhus20LasseAndNiels
    *  The program – Image project: https://teachablemachine.withgoogle.com/ 
        *  This is the link I made in the program – inserted it in the formula we got from class: https://teachablemachine.withgoogle.com/models/rTGEs-v5/ 
*  The Coding Train - 7.8: Objects and Images – p5.js Tutorial:  https://www.youtube.com/watch?v=i2C1hrJMwz0 
*  Hand images:
    *  Flat hand/paper: https://p7.hiclipart.com/preview/224/438/398/caves-of-gargas-hand-free-content-clip-art-cartoon-hands-cliparts.jpg 
    *  Fist/stone: https://png.pngtree.com/png-clipart/20190904/original/pngtree-clenched-fist-cartoon-hand-drawn-red-png-image_4492352.jpg 
    *  Peace signal/scissor: https://clipartstation.com/wp-content/uploads/2018/10/peace-sign-clipart-black-and-white-4.jpg 


