function setup() {
  // put setup code here
  createCanvas(600,600);
  background(255,0,0);
  print("hello world");
}

function draw() {
  // put drawing code here
  ellipseMode(CORNER); // Set ellipseMode is CORNER
  fill(255); // Set fill to white
  ellipse(400, 50, 100, 100); // Draw white ellipse using CORNER mode

  ellipseMode(CORNERS); // Set ellipseMode to CORNERS
  fill(100); // Set fill to gray
  ellipse(500, 25, 400, 400); // Draw gray ellipse using CORNERS mode

  ellipseMode(CORNER); // Set ellipseMode is CORNER
  fill(255); // Set fill to white
  ellipse(25, 25, 50, 50); // Draw white ellipse using CORNER mode

  ellipseMode(CORNERS); // Set ellipseMode to CORNERS
  fill(100); // Set fill to gray
  ellipse(25, 25, 50, 50); // Draw gray ellipse using CORNERS mode

}
