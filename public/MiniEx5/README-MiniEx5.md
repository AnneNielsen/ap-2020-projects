**README – MiniEx5**

URL link to my repository: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public%2FMiniEx5 

**For this MiniEx the objective was**

*  To catch up and explore further with the previous learnt functions and syntaxes
*  To revisit the past and design iteratively based on the feedback and reflection on the subject matter.
*  To reflect upon the methodological approach of "Aesthetic Programming"


![ScreenShot](Screenshot-CyclicScenery.png)

Link to RUNME: https://AnneNielsen.gitlab.io/ap-2020-projects/MiniEx5/ 


**The concept for this weeks MiniEx**

For this week’s MiniEx I wanted to develop my MiniEx1 – where I made a red house with a blue door in a snowy landscape. 
Since MiniEx1 we have learned a lot, - so in this week’s MiniEx I wanted to incorporate more knowledge about variables, syntax and functions into the source code. I also wanted to incorporate functions I haven’t used in the previous MiniEx´s before (e.g. “image”, “translate” and “rotate”). 
In this MiniEx I furthermore want to discuss, how this week’s result can be reflected upon and put in relation to some of the previous MiniEx´s themes we have been through. 



**What I did and what has changed to make this work**

I have edited a lot from the previous idea (MiniEx1: https://gitlab.com/AnneNielsen/ap-2020-projects/-/blob/master/public/MiniEx1/README-MiniEx1.md ). The only thing that remains is the structure of the house – red walls, blue door, gray roof, and three squared windows you can turn the light on and off in. I changed the season to a more summery scenery and removed the show elements. Instead we now see a blue sky and green grass (in three levels). I removed the outlines and added the “noStroke” for a more smooth and not so separated moving image. 
In the sketch from MiniEx1, you could turn the lights on and off by clicking anywhere on the screen. The feedback for my MiniEx2, was to make the color change in a figure, but only when the mouse is inside that figure (source: https://gitlab.com/AnneNielsen/ap-2020-projects/issues/2 ). 
I have incorporated that, so now you turn the lights on by placing the mouse within the window you would like to lit up. I made this function work by using if statements (inspiration: https://www.youtube.com/watch?v=r2S7j54I68c&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=16 ). In one of the windows I have placed a black shadow as well.  
When you press the mouse down, a cloud appears, - this cloud is incorporated from my MiniEx3, where we made throbbers (MiniEx3: https://gitlab.com/AnneNielsen/ap-2020-projects/-/blob/master/public/MiniEx3/README-MiniEx3.md). 
In my MiniEx1 the “house foundation” was a rectangle in a bright red color, now it is replaced with an image of red bricks,- which actually was, what the red color in MiniEx1 was supposed to symbolize.
I also got some inspiration from one of my fellow students code. Signe made a sun and a moon circulate around a house using translate and rotate,- I wanted to incorporate those elements into my sketch as well, elements I haven’t tried before(Signes source code: https://gitlab.com/Signe.Tvilling/ap-2020/-/blob/master/public/MiniEx1-1/sketch.js ). 
I changed the moon to a halfmoon and made a few adjustments on the rotation and the placing. 
To make the moon stand still in its proportion, I made it rotate the opposite direction of the rotating system with the sun around the house. 


**In relation to the previous MiniEx´s**

When looking back at previous MiniEx´s, I can see some elements or topics that can be discussed in this MiniEx as well. In MiniEx3 we made throbbers and reflected upon temporality in digital culture. This week’s MiniEx could as well be a throbber, since we have a rotating element. The only thing to make it more clear would be to add a text saying “loading…” or make percentage for the loading. But like mentioned in MiniEx3 “…a throbber does not indicate any completed or finished status and progress” (Soon, 2019). 
When making the MiniEx3 my goal was to create a throbber, that was a moving image with different interacting elements – this MiniEx fulfills this goal as well.  

The theme for the MiniEx4, capturing data, could have been incorporated in this MiniEx as well. This could have been done by adding the users face on the shadow in the window using the clm tracker. I got inspiration to this idea through my classmate Sophia´s idea in MiniEx4 (Source: https://gitlab.com/SophiaMcCulloch/ap2020/-/tree/master/public%2FMiniex4). 

In MiniEx1 we learned the basic setup of p5.js, - what was possible to do, and how to? We also had to reflect over our experience and process in programming. 
I have learned a lot since then, both in terms of the technical and conceptual elements to create a MiniEx and just in general a program in p5.js. 
I now feel like the combinations and options are endless in programming to create and unfold your ideas - I look forward to learn and explore much more. 
I find that the difficulty in using programing as a practic lies in putting different source code together (layers, functions etc.). 


I like that Aesthetic programming isn’t just about programming and writing code, but even more so, about the message behind the work and the cultural phenomena. Personally I value the meaning behind the work and the message the most, - but the source code also adds an element to the whole understanding of the program. There is several programming languages you can use, but the chosen one, in our case p5.js, also adds an aspect to the work and its message. Sometimes you explore new elements in the source code, that adds an aspect to the work, - elements that might not be mentioned in the explanation of the work. 

I feel like programming as a method in design, will be in the future. The society we live in are getting more high-tech by time and parts of our everyday life slowly gets digital as well. Computation are a trend here to stay, so instead of showing an idea on paper or a hand drawn sketch, why not go digital and program it? You might also find it easier to compose and generate ideas with the open source principle, - to view others source code, to get inspiration and generate ideas out from that or add elements from it.  




**Sources**

*  Red bricks: https://thumbs.dreamstime.com/t/pared-de-ladrillos-rojos-5672204.jpg 
*  Inspiration from Signes code – the moon and the sun: https://gitlab.com/Signe.Tvilling/ap-2020/-/blob/master/public/MiniEx1-1/sketch.js 
*  Used to add the bricks as the foundation for my house: https://p5js.org/reference/#/p5.Image/resize 
*  Soon, Winnie. "Throbber: Executing Micro-temporal Streams", Computational Culture, 2019.
*  Sophia’s code: https://gitlab.com/SophiaMcCulloch/ap2020/-/tree/master/public%2FMiniex4).
*  My MiniEx1: https://gitlab.com/AnneNielsen/ap-2020-projects/-/blob/master/public/MiniEx1/README-MiniEx1.md
*  My MiniEx3: https://gitlab.com/AnneNielsen/ap-2020-projects/-/blob/master/public/MiniEx3/README-MiniEx3.md 
*  Geoff Cox and Winnie Soon, 2020 (aesthetic programming and open source publishing): https://gitlab.com/siusoon/Aesthetic_Programming_Book/-/tree/master/source/0-Preface 
*  Feedback from MiniEx2: https://gitlab.com/AnneNielsen/ap-2020-projects/issues/2 
*  Inspiration to make the if statements – the coding train: https://www.youtube.com/watch?v=r2S7j54I68c&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=16 
