//Defining and giving functions value:
var M = 0; //Function for the clouds to make it float on one line (x) from left to right.
var Q = "• Press and hold to make the cloud appear.";
var yellow; //to define a function for the color yellow(lights)
let img;


function preload() { //loading the image
  img = loadImage('https://thumbs.dreamstime.com/t/pared-de-ladrillos-rojos-5672204.jpg');
}

function setup() {
  // put setup code here
  createCanvas(500,500);

  frameRate(20) //The speed - amount of times it refreshes a second.

  yellow = color(255, 204, 0); //giving value to the color yellow (the lights)

  img.resize(150, 150);
}

function draw() {
  // put drawing code here

// The background - the black sky
  background(50,100,210);
  let t = frameCount / 60; // update time


  push();//the sun and moon operate seperately in turning

//the Sun
  noStroke();
  fill("Yellow");
  translate(275, 325);
  rotate(frameCount / 100.0);
  star(225, 0, 50, 25, 20);


//the moon
  moon(200,0);

  pop();

//the foundation of the house - color
  image(img, 200, 250);

// the color of the roof
    fill(150);
//the roof of the house
    triangle(360,250,280,200,190,250);

//no outline
  noStroke();

//the color of the three windows - blue, like the background
    fill(50,100,210);
//the 3 windows
    square(217,270,50);
    square(283,270,50);
    square(283,333,50);


//the color of the door
  fill('blue');
 // A rectangle - the door
  rect(220, 333, 43, 67);

//the color of the doorknob
fill(255);
// An ellipse - the doorknob
ellipse(227, 365, 7, 7);


//the color of the ground - 3 grass levels
  fill(64, 176, 56);
 // A rectangle - the ground
  rect(0, 400, 500, 35);

  fill(60, 161, 53);
  rect(0, 435, 500, 35);

  fill(59, 140, 53);
  rect(0, 465, 500, 35);

//to make the lights rurn on in the windows
if(mouseX > 217 && mouseX < 267 && mouseY > 270 && mouseY < 320) {
  fill(yellow)
  square(217,270,50);
   }
if(mouseX > 283 && mouseX < 333 && mouseY > 270 && mouseY < 320) {
  fill(yellow)
  square(283,270,50);
   }
if(mouseX > 283 && mouseX < 333 && mouseY > 333 && mouseY < 383) {
  fill(yellow)
  square(283,333,50);
  fill(0);
  rect(310,363, 17, 20, 5); //the body of the person
  ellipse(318.5, 356, 15, 15); //the head of the person
   }


//The cloud appearing when the mouse is pressed - boolean variable
   if (mouseIsPressed) {

    // The could
    fill(250) //the color of the could
    ellipse(M+20,50,70,70)
    ellipse(M,20,40,50)
    ellipse(M-80,40,20,20)
    ellipse(M-80,55,60,60)
    ellipse(M-30,40,70,70)
    rect(M-80,40,100,45)

    //To make the cloud loop, start from 0, when it has passed by/made it to the end of the canvas
    if (M > width) {
      M = 0
    }
  M = M + 2 //every frame the cloud moves with 1 (how fast it moves)

  }

} //End of draw


function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints; // npoints = number of points/triangles in the sun
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      //to create the star shape nicely (creating the "triangles")
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy); //points out
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy); //points in
    }
    endShape(CLOSE);
  }
//The Moon: x = 200 & y = 0
function moon(x,y){
  push();

  //to make the moon stand still - rotate the other way
  translate(-x, y);
  rotate((frameCount / 100.0)*-1);

  //the big ellipsis
  noStroke();
  fill(247, 238, 156);
  ellipse(0, 0, 70, 70);

  //the small ellipsis
  fill(50,100,210); //the circle is the same color as the background - to create the half moon
  ellipse(20, 0, 50, 50);

  pop();

}
