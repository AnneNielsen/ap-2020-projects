**README – MiniEx3**


URL link to my repository: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public/MiniEx3 


For this MiniEx the objective was: 

* To reflect upon temporality in digital culture with the throbber icon. 
* To experiment with various computational syntax and effects of animation and transformation.

For this week’s MiniEx we had to redesign and program an animated moving throbber. 
I got my inspiration to create my throbber through the Danish weather this month (start of spring), the work of samchasan on p5.js (https://editor.p5js.org/samchasan/sketches/rJRvYKTq-) and the required read text: Soon, Winnie. "Throbber: Executing Micro-temporal Streams", Computational Culture, 2019. 
Winnie Soon text is about throbbers - what they are, what their intension is and all the technological processes of data behind a throbber – a “Throbber” is a graphically animated icon, that tells the users, that something is loading-in-progress. 


**My idea and thinking process on: time-related syntaxes/functions in my program, and why have I used them in this way? How is time being constructed in computation (refer to both the reading materials and your process of coding)?**

Time is a difficult thing to grasp. In everyday life we try to track time with hours, minutes, seconds etc. On a computer we try to do the same thing, by adding a clock on the screen for us to follow. Throbbers don’t follow that way of tracking time - “…a throbber does not indicate any completed or finished status and progress” (Soon, 2019). This can be so very frustrating, since you wait for an unknown period of time. The loading process depends on the (internet)connection and accessibility of the data you want to load,- a throbbers is therefore very hard to track in time. 

Throbbers comes in many forms and shapes, and they have different ways in “…speculating and reflecting on how time is being structured and organized” (Soon, 2019) – some have circulation, movement, numbers/percentage, and some sort of “loading bar”. 

For this week’s miniEx I have created a rainbow throbber with the percentage of the loading, a blue sky, green grass and a cloud passing by, when the mouse is pressed. 
My throbbers time-related syntaxes/functions are the percentage of the loading (shown in the middle of the rainbow) and the rainbow getting more and more clear, as it completes the loading. You can also argue that the sky passing by (when you press down the mouse) is a time-related function, since you might get the time to see it pass by (= a slow loading progress).
In a loading process, I personally prefer loading bars, - since you can see the amount of time left visually, how much of the “scale” there is left to be colored. But since it is hard to calculate the exact time left, you can’t really rely on the loading bars either. I tried to incorporate those two thoughts into my throbber by making the rainbow, that starts off by being just a shade, and then by time gets more and more “colored in”. The coloring process of the rainbow symbolizes the appearance of the page, that you are about to see – it gets more and more colored in. 



**My thoughts on: a throbbers in digital culture, what a throbber communicates, and/or hides? How might we characterize this icon differently?**

I consider throbbers, a way to simplify all the processes working behind the loading process – user friendliness covering up the bigger data technological process behind, and trying to visualize it in a way for us to understand, that the loading is in progress. In your every day we easily get annoyed or frustrated, when throbbers appear. 
Many throbbers are very simple in style, and basically just a simple service message we only can use to tell us, that the content is loading. But what if the throbber is interactive, wouldn’t that make the process less annoying and frustrating? If you have to wait anyway, why not have the opportunity to do something while waiting? 
I have tried to incorporate that thought into my redesign of a throbber,- you can make a cloud move and the throbber is not just a circle (like most others), it’s a whole moving image. 
The cloud is just an example of an interaction with a throbber, you can add other elements too,- maybe make a throbber game to play while waiting (like the no connection game on Google: https://www.youtube.com/watch?v=CnYykc6cdgo)? 


![ScreenShot](Screenshot-MiniEx3-rainbow.jpg)
Link to RUNME: https://AnneNielsen.gitlab.io/ap-2020-projects/MiniEx3/ 


**My programming diary - Sources used in my throbber:**


* To make the ellipsis center in my canvas, “ellipseMode” - https://p5js.org/reference/#/p5/ellipseMode  
* To slow down the framerate (to be able to see the rainbow fade and the percentage counting) - https://p5js.org/reference/#/p5/frameRate 
* To have no outline of the elements (to create a better fading) - https://p5js.org/reference/#/p5/noStroke 
* To repeat my loop of transparentcy, I got inspiration from the required video – “4.1: While and for Loops by Daniel Shiffman, Code! Programming with p5.js, The Coding Train”: https://www.youtube.com/watch?v=cnRD9o6odjk&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=18 
* To create my Boolean variable, “mouseIsPressed” I got inspiration from the video – “Notes to video 3.4: Boolean Variables by Daniel Shiffman, Code! Programming with p5.js, The Coding Train ”: https://www.youtube.com/watch?v=Rk-_syQluvc&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=17 
* To make the counting for the percentage I used the function “map” - https://p5js.org/reference/#/p5/map 



**Other sources: **

*  Soon, Winnie. "Throbber: Executing Micro-temporal Streams", Computational Culture, 2019.
*  The work of samchasan: https://editor.p5js.org/samchasan/sketches/rJRvYKTq-

