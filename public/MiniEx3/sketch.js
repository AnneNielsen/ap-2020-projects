//Defining and giving functions value:
var X = 0; //Function for the rainbow to make it fade from transparent (alpha) to "full" color
var M = 0; //Function for the clouds to make it float on one line (x) from left to right.
var Q = "• Press and hold to make the cloud appear.";


function setup() {
  createCanvas(400, 400);

  frameRate(20) //The speed - amount of times it refreshes a second.
}


function draw() {
  background(90,130,250); // The blue sky (1/4 elements)

	  ellipseMode(CENTER); //to center all the ellipses

  noStroke() //no outline

  //To make the rainbow fade into less transparency
  X = X+1
  if(X > 255) {
    X = 0
  }

  // The blue sky 1 - the outside of the rainbow (2/4 elements)
  fill(120,140,250)
  rect(0,200,400,50)
  fill(60,110,200)
  rect(0,0,400,120)



  // THE RAINBOW

  //violet
  fill(100,0,200,X)
  ellipse(200,200,320,320)
  //indigo
  fill(150,0,200,X)
  ellipse(200,200,300,300)
  //blue
  fill(0,150,250,X)
  ellipse(200,200,280,280)
  //green
  fill(20,250,20,X)
  ellipse(200,200,260,260)
  //yellow
  fill(250,250,0,X)
  ellipse(200,200,240,240)
  //orange
  fill(250,150,40,X)
  ellipse(200,200,220,220)
  //red
  fill(240,20,20,X)
  ellipse(200,200,200,200)

  //The blue sky 2 - the inside of the rainbow (1/4 elements)
  fill(90,130,250)
  ellipse(200,200,180,180)

  //The green grass - three levels
    //(the grass covers the rest of the circle,to make it look like a rainbow).
  fill(100,200,40)
  rect(0,250,400,300)
  fill(80,190,20)
  rect(0,250,400,80)
  fill(50,150,30)
  rect(0,250,400,30)


//The cloud appearing when the mouse is pressed - boolean variable

  if (mouseIsPressed) {

    // The could
    fill(250) //the color of the could
    ellipse(M+20,50,80,80)
    ellipse(M,20,40,60)
    ellipse(M-80,40,30,30)
    ellipse(M-80,55,70,70)
    ellipse(M-30,40,80,80)
    rect(M-80,40,100,50)

    //To make the cloud loop, start from 0, when it has passed by/made it to the end of the canvas
    if (M > width) {
      M = 0
    }

  }

  M = M + 2 //every frame the cloud moves with 1 (how fast it moves)

  counting(); //using the function "counting" in draw (the percentage).

}

function counting() {
  let m = floor(map (X, 0, 255, 0, 100));

  textSize(20);
  fill (80,20,80);
  text(m,185,200);

  // the percentage sign
  fill (80,20,80);
  text("%",210,200);

}
