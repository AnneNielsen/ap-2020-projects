**README – MiniEx2**

For this MiniEx the objective was: 
*  To experiment with various geometric drawings, explore possibilites, especially on shapes and drawing with colors.
*  To reflect on a politics of emojis via the assigned texts.



**Service-note**

My sketchfiles is under the folder "public":
*  Black smiley: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public/RUNME-MiniEx2-BalckSmiley
*  Danish student cap: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public/RUNME-MiniEx2-Cap 

Link to the rest of my work README etc: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public/MiniEx2 


**My thoughts on: How I would put my emojis in a wider cultural context that concerns representations, identity, race, social, economics, culture, device politics and beyond?**

In this weeks MiniEx we had to design two emoji icons and reflect upon how emoji relate to identity and cultural phenomena.

I got my inspiration to create the two emojis out from the read text - Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal.", 2018, p.35-51. 


![ScreenShot](Screenshot-MiniEx2-blacksmiley.png)

Link to RUNME - Black smiley:
https://AnneNielsen.gitlab.io/ap-2020-projects/RUNME-MiniEx2-BalckSmiley/ 


**Black smiley**

In the test “Modifying the Universal “ by Femke Snelting  et al., there was a huge discussion about emojis having another colour than the yellow colour, earlier considered as neutral or as a base,– in general emojis being coloured as skin tones.  
This debate developed through Apple releasing their new emojis, where people realised, that the emojis weren’t yellow blobs anymore, but that they actually were white people. Some people didn’t feel represented anymore. Apple then shortly after changed the emojis to the yellow that people know emojis as being or added a skin-color changing function to the emojis (several options). 

But the standard emojis also known as smileys are still yellow, you can’t choose their skin colour. I therefore wanted to explore that thought,- so I have created a black skinned smiley. 
Take a look at it. 
Do you feel represented even though it is black and maybe not the same colour as you? Can you identify yourself with this emoji?
I am also wondering, why the yellow colour is considered “neutral” and not the white or black?


**My programming diary – making the black smiley**

My goal was to make it look as much like Apples – the happy one with big eyes and a big smile. 
*  To create this smiley I started off by creating a big ellipse (to make the face) and then added the brown skin color (“fill(120,70,0);”). 
    *  Then I made a “stroke”, to make the outline of the face 

*  Then I created the eyes with two elongated shaped ellipses 
    *  I colored them a darker brown than the face (lower numbers in color). 

*  Then I got inspiration from the reference page on p5.js to create the mouth as an arch (actually two arches combined). 
    *  I played around with the techniques in the two sources and got the result I wanted in the end – I figured that I needed to add “PI + TWO_PI, CHORD”.
        *  Sources used: https://p5js.org/reference/#/p5/arc & https://p5js.org/examples/form-shape-primitives.html 
        *  I made one of the arches the same dark brown as the eyes and the other white representing the teeth (like in the original emoji). 



![ScreenShot](Screenshot-MiniEx2-cap.png)

Link to RUNME - Danish student cap:
https://AnneNielsen.gitlab.io/ap-2020-projects/RUNME-MiniEx2-Cap/ 


**The Danish student cap**

In the read text by Femke Snelting  et al., they also mention that, when the discussion started about skin tone modifications, there was no way back. The beginning of applying skin tone started a focus on other areas too e.g. age, gender, religion etc. as well. 
I have explored that thought by incorporation religion and culture into an emoji. 

What I have created is a Danish “student cap”, in Danish called “studenterhue”. The Danish student caps goes way back in Danish history - since 1856. It is worn as a symbol for graduating the higher education level, that we call the gymnasium (in danish: “gymnasiet”) - e.g. “STX”, “HHX” and “HTX”.
Each gymnasium education have its own colour ribbon around the hat, so I have tried to incorporated that into my emoji as well. 
I have designed this emoji to reflect over the cultural phenomena. If we first start to change colour, why not incorporate culture, norms, religions ect. as well? Like they expressed in the text, this can go on, and there is no way back. 
There is already an emoji for the “traditional” graduation cap (used in America and UK), so why not make a Danish version of it as well? 


**My programming diary – making the Danish student cap**

*  The Danish student cap contains squares with rounded corners – used in the red “ribbon”, the “linen”, the “chinstrap” and to make the cross in the brooch. 
    *  Source: https://p5js.org/reference/#/p5/square

*  To make the cap (“the shadow”) I used the same technique as in the black smiley mouth: “arc(-, -, -, -, 0, PI + TWO_PI, CHORD);”. 
    *  Sources used: https://p5js.org/reference/#/p5/arc & https://p5js.org/examples/form-shape-primitives.html 

*  This emoji contains several ellipses as well – used to make the two silver buttons and the three ellipses to make the brooch. 
    *  Source: https://p5js.org/reference/#/p5/ellipse 

*  The function “mousePressed” makes the color in the “ribbon” and “brooch” change color – it can change in between 3 colors by using “else if”: red/bordeaux (STX), dark blue/”royal blue” (HHX) and dark navy blue(HTX). 
    *  Sources: 
        *  https://p5js.org/reference/#/p5.Element/mousePressed 
        *  https://www.youtube.com/watch?v=Bn_B3T_Vbxs 



**Sources:**

*  The story behind the danish “studenterhue” – it’s in Danish (not available in English on Wiki):  https://da.wikipedia.org/wiki/Studenterhue_(Danmark) 
*  Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Execu=ng Prac=ces, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humani=es Press, 2018, 35-51. or Femke Snel=ng, Modifying the Universal, MedeaTV, 2016 [Video, 1 hr 15 mins]. 
*  The coding train - 2.2: Variables in p5.js (Make your own) - p5.js Tutorial: https://www.youtube.com/watch?v=Bn_B3T_Vbxs 
*  For inspiration:
    *  https://p5js.org/examples/ 
    *  https://p5js.org/reference/ 
