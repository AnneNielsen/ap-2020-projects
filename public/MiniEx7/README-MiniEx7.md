**README – MiniEx7**

**For this MiniEx the objectives was:**

*  To implement a rule-based generative program from scratch.
*  To strengthen the computational use of loops and conditional statements in a program.
*  To reflect upon the concept of automatism conceptually and practically.

URL link to my repository: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public%2FMiniEx7 

![ScreenShot](Screenshot-Circle.png)

Link to RUNME: https://AnneNielsen.gitlab.io/ap-2020-projects/MiniEx7/ 

**My generative artwork**

For this week’s MiniEx my idea was to create a generative artwork with two main rules:
1.  That it will draw circles repeatedly from the middle. 
2.	That each circle being drawn will fade more in its color,- meaning the first circle has full color and the last circle drawn will almost be transparent. 

I got inspiration to create this artwork through the artist Julien Leonard’s artworks (http://julienleonard.com) and the “circle packing technique” in the assigned video for this week (Marius Watz, "Beautiful Rules: Generative Models of Creativity", 2007). The circle packing technique is about fitting as many circles as possible into a limited area.
I wanted to create a generative artwork since it: “…explores new creative expressions based on computation” (Marius Watz, 2007) .

When starting the program, the circles starts to draw from the middle and out, slowly fading out in their color (using alpha). When the program has drawn all the possible circles within the canvas, the console types “finished” and the loop of drawing circles stops. 

When I started to code my program, I added a couple of rules for the program to perform within the range I wanted it to. I wanted the viewer to be able to see all the circles drawn(within the canvas shown), and didn’t want the circles to overlap each other. I therefore added conditional statements and variables to my code to avoid that (take a look inside my sketch).

Even though there are a set of rules for the program, the program can still variate in its outcome. Every time you load the program, the outcome will be different because of my use of “width”, “height”, “this.x”, “this.r” etc. instead of using a certain number.  

To Auto generate something is to set up “rules”, a “recipe”, a “formula”, a “system” or a “script” for the computer to automatically follow in generating something – how the rules are performed and how they can produce random results. 
Through these “rules” you can control the result of the product you want. When nothing is being typed, all options and possibilities for the outcome is still available. By adding conditional statements, variables, functions and other pieces of code, you can control and narrow the possible random result. 

In the video “Beautiful Rules: Generative Models of Creativity” by Marius Watz, he talks about the computer contributing to create the final outcome as being a “co-author” (Marius Watz, 2007).
I experimented with this in this week’s MiniEx by adding code with a wide range of possibilities e.g. “random” or “this.x”. This makes the process of creating the final outcome and product a lot more interesting since the result can vary, depending on the boundaries you have added through code. In the text “Randomness” they also discuss this “contribution” of randomness and what interesting outputs a computer can generate through that (Montfort, N, et al, 2012). 
This was something I wanted to explore since: “The first two exhibitions of computer- generated graphics appeared in art galleries in 1965; both shows included pieces that were created using random values.” (Montfort, N, et al, 2012, p. 135). 

This artwork could have been further developed by making the drawing of circles, circulate around in different patterns,- maybe slalom into the middle like in Julien Leonard’s artwork “Blue Hole” from 2018 (http://julienleonard.com). 


**Sources**

*  Montfort, N, et al. "Randomness". 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, The MIT Press, 2012, pp. 119-146 (The chapter: Randomness)
*  [1 hour video] Marius Watz, "Beautiful Rules: Generative Models of Creativity", in The Olhares de Outono (2007), https://vimeo.com/26594644.
*  Drew inspiration to the code through the video “Coding Challenge #50.1: Animated Circle Packing” with The Coding Train: https://www.youtube.com/watch?v=QHEQuoIKgNE 
*  Inspiration from: 
    *  The artist Julien Leonard: http://julienleonard.com 
    *  Another programmers code: https://editor.p5js.org/cah689/sketches/B1kCFI36b 
