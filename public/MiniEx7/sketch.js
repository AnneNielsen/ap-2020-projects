// Defining all circles
var circles = [];

var spread = 0; //Variabel that controls the distance from the middle, in which the circles can be drawn

function setup() {
  createCanvas(500, 500);
  // Start with a big one in the center in the hopes that it
  // takes up a lot of a space and the sketch runs faster
  circles.push(new Circle(width / 2, height / 2, min(width, height) / 20));
  spread = circles[0].r+20; //The distance from radius of the big circle + 10, for space from start to draw circles on.

}

function draw() {
  background(0);
  //Adding the spread with +1, but this is only possible, if it is within canvas.
  if (spread < dist(width / 2, height / 2, width, height)){
    spread += 0.5;
  }

  // All the circles
  for (var i = 0; i < circles.length; i++) {
    var c = circles[i];
    c.show(i);

    // Growing circles
    if (c.growing) {
      c.grow();
      // No to overlap any previous circles
      for (var j = 0; j < circles.length; j++) {
        var other = circles[j];
        if (other != c) {
          var d = dist(c.x, c.y, other.x, other.y);
          if (d - 1 < c.r + other.r) {
            c.growing = false;
          }
        }
      }

    // To make the circles stop at an edge (go no furtherout).
      if (c.growing) {
        c.growing = !c.edges();
      }
    }
  }

  // Let's try to make a certain number of new circles each frame
  // More later
  var target = 1 + constrain(floor(frameCount / 200), 0, 20);
  // How many
  var count = 0;
  // Try N times
  for (var i = 0; i < 1000; i++) {
    if (addCircle()) {
      count++;
    }
    // We made enough
    if (count == target) {
      break;
    }
  }

  // To make the loop stop if it has run through - don´t make any more
  if (count < 1) {
    noLoop();
    console.log("finished");
  }
}

// Add one circle
function addCircle() {
  // Here's a new circle
  var newCircle = new Circle(random(width), random(height), 1);
  // Controling the spot
  var distSpread = dist(width/2, height/2, newCircle.x, newCircle.y);
  for (var i = 0; i < circles.length; i++) {
    var other = circles[i];
    var d = dist(newCircle.x, newCircle.y, other.x, other.y);
    if (d < other.r + 4) {
      newCircle = undefined;
      break;
    }
  }
  // If one circle is added, add one more
  if (newCircle && distSpread < spread) {
    circles.push(newCircle);
    return true;
  } else {
    return false;
  }
}

// Circle object
function Circle(x, y, r) {
  this.growing = true;
  this.x = x;
  this.y = y;
  this.r = r;
}

// Check that the circles get´s stuck to an edge,- fills the canvas
Circle.prototype.edges = function() {
  return (this.r > width - this.x || this.r > this.x || this.r > height - this.y || this.r > this.y);
}

// Grow
Circle.prototype.grow = function() {
  this.r += 0.5;
}

// Show
Circle.prototype.show = function(i) {
  noFill();
  strokeWeight(2);
  var alphaA = map(i,0,1000,255,0);
  //fill();
  stroke(255,alphaA);
  ellipse(this.x, this.y, this.r * 2);
}
