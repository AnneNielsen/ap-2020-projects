var colorRed; //defining the function
var colorChange = 0; //defining variable for the function to change color and giving it value

function setup() {
  createCanvas(400, 400);

colorRed = color(250,0,0);//giving the function colorRed a value

}

function draw() {
  background(247);

//The danish student cap

  //the "cap-shadow"
  stroke(0); // color of the outline
  strokeWeight(2); //thickness of the outline
  fill(0); // the interior of the ellipse
  arc(200, 260, 280, 150, 0, PI + TWO_PI, CHORD);
    // CHORD = closes the ellipse with an outline
    //TWO_PI = "cuts the ellipse up", so its not a full ellipse


  //the ribbon (changeable in color if mouseclicked)
  noStroke(0); //no outline
  fill(colorRed); // the interior of the ellipse to be the defined color = red
  rect(50, 210, 300, 60, 10); // Draw a rectangle with rounded corners, each having a radius of 20.

  //The white linen
  stroke(75, 200); //the color of the outline (grayscale & alpha)
  strokeWeight(1); //the thickness
  fill(245); //the color of the interior
  rect(46, 140, 306, 80, 10); // Draw a rectangle with rounded corners.

  //the black chinstrap
  stroke(0);//the color of the outline
  strokeWeight(1); //the thickness of the outline
  fill(0); //the color of the interior
  rect(46, 245, 308, 15, 10); // Draw a rectangle with rounded corners.

  //"symbol-brooch" in the linen
    //red "outer circle"
    stroke(150); //the color of the outline
    strokeWeight(1); //the thickness of the outline
    fill(colorRed); //the color of the interior - the defined color = red
    circle(200, 180, 45);
    //The silver threads
    fill(230); //the color of the interior
    circle(200, 180, 40);
    //the red middle
    fill(colorRed); //the color of the interior - the defined color = red
    circle(200, 180, 32);

  //silver buttons
  stroke(75, 200); //the color of the outline
  strokeWeight(1); //the thickness of the outline
  fill(230); //the color of the interior
  ellipse(46, 252, 5, 18);
  fill(230); //the color of the interior
  ellipse(354, 252, 5, 18);

  //cross on brooch
  noStroke(); //no outline
  fill(230); //the color of the interior
  rect(191, 176, 20, 5, 1);
  rect(198, 168, 5, 24, 1);
}

function mousePressed(){

colorChange = colorChange+1;//for it to add one every time it loops

if (colorChange == 1){ //using the variable for the cap to change color
  colorRed = color(0,0,125); //what color it is
  } else if(colorChange == 2){ //what color it changes into
    colorRed = color(200,0,0);
  } else if(colorChange == 3){ //what color it changes into
    colorRed = color(10,0,75);
  colorChange = 0; //for it to loop back to the start of the value
}


}
