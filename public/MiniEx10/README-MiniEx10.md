## README – MiniEx10

**For this MiniEx the objectives was:**

*  To acquire the ability to decompose/break down a computer program into its definable parts and relations
*  To organize and structure a computer program through a flowchart
*  To understand flowchart as a means for communication and planning, and tool for critical thinking
*  To understand the concept of algorithms from both computer science and cultural perspectives

URL link to my repository: https://gitlab.com/AnneNielsen/ap-2020-projects/-/tree/master/public/MiniEx10

#### The two ideas for the final project is made in collaboration with:
Link to my group members sites
*  Simon: https://gitlab.com/SimonVanNguyen
*  Pernille: https://gitlab.com/pernwn
*  Torvald: https://gitlab.com/Teepeek

#### My flowchart on MiniEx8 (made in collaboration with Pernille)
Link to project: https://gitlab.com/AnneNielsen/ap-2020-projects/-/blob/master/public/MiniEx8/README-MiniEx8.md 

![ScreenShot](MiniEx10-individual.jpg)

#### Flowchart over idea 1 

![ScreenShot](MiniEx10-IDEA1.jpg)

#### Flowchart over idea 2

![ScreenShot](MiniEx10-IDEA2.jpg)


## MiniEx10 - 3 Flowcharts 

This week’s MiniEx is quite different from the previous ones, for the first time we were asked not to make programs to run,- but instead make flowcharts of one previous technically complex MiniEx program and two flowcharts visualizing ideas for our groups final project where the algorithmic processes was in focus. 
Breaking it down I made a flowchart of a project, that I already finished and two flowcharts of a project, that I haven’t even started programming. 

According to Nathan Ensmeger flowcharts are used to represent the conceptual structure of a complex software systems (Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.",  2016, pp. 321). The original intension for flowcharts, when they were introduced to programming in the mid 1940s was, that “A programmer (or “coder”) would then translate that flowchart into the machine language understood by the computer, - in other words the flowchart was produced before programming the actual program. But the reality was different then, than the intension – many programmers produced their flowcharts only after they were done writing code, and the opinions about making flowcharts at all was very separated ”(Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.",  2016, pp. 322). 

In this week’s MiniEx we have tried both these approaches in using flowcharts – before and after making the program. I personally find it a lot easier to create the flowchart after the final program has been created,  because you then know that the program works the intended way and not just claiming that it will work. When programming unexpected errors and technical challenges often appear in programming your idea,- then you tangle with it and change it until it works and you are satisfied with it. I therefore don’t consider making a flowchart before programming the final idea. Since we haven’t started programming the two ideas for the final project, it is hard to say what the technical challenges for the two ideas will be and how to address them? 
In our 1st Idea the main focus will be on finding and linking the program to a good API, this can be quite challenging and time consuming, especially if you then also want to customize it in the HTML-file as well (we also experienced this in our MiniEx9). Another challenge in Idea 1 is to make the results from the API appear in a random way, which is the intension to make it look like the “waiter”/the program decides, what to show of dishes. 
In our 2nd idea I think the biggest challenge will be to find and connect sound files to each of the 6 buttons in the program. Another challenge in idea 2 will be to make the program memorize and in the end create its own melody. Actually the whole program is quite complex and might not even be possible for us to develop with the programming experience we have. We definitely need help for this, research online and ask around. Maybe this is too crazy to perform at our stage? 

In this week’s MiniEx I experienced the two flowcharts as a good icebreaker for creating an idea, something to build upon and develop out from, until the goal is reached – a starting point. 

I decided to create a flowchart over my MiniEx8, that I created with Pernille(Link to her profile and our MiniEx: https://gitlab.com/pernwn/ap2020/-/blob/master/public/MX8/README.md ) – I find this one of the most complex ones, that I have made yet, - since we programmed the JSON file ourselves and coupled that to our JavaScript sketch. 
That something you have worked on for so long, can be broken down to something so simple as a flowchart can be frustrating seen from the programmers perspective.  The technical complexity, and many of the programs details is being left out in the flowchart for everybody to understand it, which I to a certain point find a shame not to present . In this sense it is important then, to look at the functionality of a flowchart, what it will be used for, to understand and use it to its full potential. Flowcharts is being used in a social and organizational matter as a visualization of a program and as an explanatory tool to break down hard software and code(Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.",  2016, pp. 325) – e.g. used as a communication tool between programmers and managers, human to human communication (Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.",  2016, pp. 338). 
Nathan Ensmeger, when talking about the history of flowcharts also mentions flowcharts as being:“…visual documentation that facilitated understanding, memory, and conversation”.(p. 339). 
There is according to Nathan Ensmeger, two important purposes of a flowchart: making a program clear to someone who wishes to know about it and aiding the programmer himself to check that the program as written does the required job” (Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.",  2016, pp. 340). 
The challenge is to compose a flowchart that encapsulates the functions of the program (the algorithmic procedure) and at the same time is simple enough to be understandable to the level of communication with a “non-programmer”. 
 If the flowchart gets to detailed or technical and complex like the program, it isn’t useful anymore, and then it is better to look at the actual code, which not everybody is able to understand (Ensmenger, Nathan. "The Multiple Meanings of a Flowchart.",  2016, pp. 333). 
I find the second purpose of the flowchart doubtful in practice, since I like other programmers Ensmenger mentions in the text, find it time-consuming. The flowchart worked well as a starting point for the group, but I personally would need a good reason to maintain the flowchart, e.g. to show the final product to a manager, otherwise I probably wouldn’t make use of it. 

I personally see flowcharts as some sort of “visual algorithm”, since both algorithms and flowcharts: “…come with certain assumptions and values about the world(Bucher, Taina. If...THEN: Algorithmic Power and Politics, 2018, pp.23). 
The difference is here that the algorithm is acting upon this assumption and the flowchart isn’t,- maybe this is one of the reasons for people being torn about the use of flowcharts? Questioning the validity of flowcharts?


**Sources**

*  Ensmenger, Nathan. "The Multiple Meanings of a Flowchart." Information & Culture: A Journal of History, vol. 51 no. 3, 2016, pp. 321-351. Project MUSE, doi:10.1353/lac.2016.0013 (check AU e-library)
26 pages 
*  Bucher, Taina. If...THEN: Algorithmic Power and Politics, Oxford University Press, 2018, pp. 19-40 (The chapter called "The Multiplicity of Algorithms") (check blackboard > literature)
22 pages (12 pdf pages)
*  My MiniEx8: https://gitlab.com/AnneNielsen/ap-2020-projects/-/blob/master/public/MiniEx8/README-MiniEx8.md 

